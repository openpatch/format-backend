# Setup/Getting started

- Prerequisites (1): [docker](https://www.docker.com/) (version: 19.03.8) and [docker-compose](https://docs.docker.com/compose/) (version: 1.25.5) are installed on your system
- Prerequisites (2): You need to have a github account in order to pull the images of the connected services

```
git clone https://gitlab.com/openpatch/format-backend.git
cd format-backend
git checkout feature/format-sql
```

- Create a token on github: https://github.com/settings/tokens (the token has to have the rights to download docker images/packages. Be sure to select the scope `read:packages`)
- Login with this token and pull runner-sql image

```
docker login -u <UserName> -p <Token> docker.pkg.github.com
docker pull docker.pkg.github.com/laurakeil1/runner-sql/runner-sql:1.0
```

- Run services:

```
docker network create openpatch-runner-net
docker-compose up
```

# Sample requests/responses

- /validate endpoint checks whether a created item (especially query1) is valid or not
- /evaluate endpoint checks whetehr a provided solution (query2) is correct or not
- All sample requests are included in the following Postman collection: https://www.postman.com/collections/a75c27a2a7ba3f76c267 (you can just open the link, copy everything and use the import feature in Postman)

## /validate endpoint

### /validate - task schema invalid

```json
curl --location --request POST 'localhost:5007/v1/validate' \
--header 'Content-Type: application/json' \
--data-raw '{
	"format_type": "sql",
	"evaluation": {
		"skip": false,
		"query1": {
			"source": "SELECT * FROM customers LIMIT 1"
		}
	}
}'
```

```json
{
  "correct": false,
  "details": {
    "error": {
      "data": ["Missing data for required field."]
    }
  }
}
```

### /validate - task schema valid - query1 missing

```json
curl --location --request POST 'localhost:5007/v1/validate' \
--header 'Content-Type: application/json' \
--data-raw '{
	"format_type": "sql",
	"data": {},
	"evaluation": {
		"skip": false
	}
}'
```

```json
{
  "correct": false,
  "details": {
    "error": {
      "evaluation": {
        "query1": ["Missing data for required field."]
      }
    }
  }
}
```

### /validate - task schema valid - query1 invalid 1

```json
curl --location --request POST 'localhost:5007/v1/validate' \
--header 'Content-Type: application/json' \
--data-raw '{
	"format_type": "sql",
	"data": {},
	"evaluation": {
		"skip": false,
		"query1": {
            "source": " * FROM customers"
        }
	}
}'
```

```json
{
  "correct": false,
  "details": {
    "error": {
      "message": "Query is not a SELECT-statement",
      "queryId": "query1"
    }
  },
  "task": {
    "data": {},
    "evaluation": {
      "query1": {
        "source": " * FROM customers"
      },
      "skip": false
    }
  }
}
```

### /validate - task schema valid - query1 invalid 2

```json
curl --location --request POST 'localhost:5007/v1/validate' \
--header 'Content-Type: application/json' \
--data-raw '{
	"format_type": "sql",
	"data": {},
	"evaluation": {
		"skip": false,
		"query1": {
			"source": "SELECT * FROM person"
		}
	}
}'
```

```json
{
  "correct": false,
  "details": {
    "error": {
      "code": "SQLITE_ERROR",
      "errno": 1,
      "message": "SQLITE_ERROR: no such table: person",
      "queryId": "query1"
    }
  },
  "task": {
    "data": {},
    "evaluation": {
      "query1": {
        "source": "SELECT * FROM person"
      },
      "skip": false
    }
  }
}
```

### /validate - task schema valid - query1 valid

```json
curl --location --request POST 'localhost:5007/v1/validate' \
--header 'Content-Type: application/json' \
--data-raw '{
	"format_type": "sql",
	"data": {},
	"evaluation": {
		"skip": false,
		"query1": {
			"source": "SELECT * FROM customers LIMIT 1"
		}
	}
}'
```

```json
{
  "correct": true,
  "task": {
    "data": {},
    "evaluation": {
      "query1": {
        "source": "SELECT * FROM customers LIMIT 1"
      },
      "skip": false
    }
  }
}
```

## /evaluate endpoint

### /evaluate - solution schema invalid

```json
curl --location --request POST 'localhost:5007/v1/evaluate' \
--header 'Content-Type: application/json' \
--data-raw '{
	"task": {
		"format_type": "sql",
		"evaluation": {
			"skip": false,
			"query1": {
				"source": "SELECT * FROM customers LIMIT 1"
			}
		}
	}
}'
```

```json
{
  "correct": false,
  "details": {
    "error": {
      "current_source": ["Missing data for required field."],
      "sources": ["Missing data for required field."]
    }
  }
}
```

### /evaluate - solution schema valid - query2 invalid 1

```json
curl --location --request POST 'localhost:5007/v1/evaluate' \
--header 'Content-Type: application/json' \
--data-raw '{
	"task": {
		"format_type": "sql",
		"evaluation": {
			"skip": false,
			"query1":
				{
					"source": "SELECT * FROM customers LIMIT 1"
				}
		}
	},
	"solution": {
		"current_source": 0,
		"sources": [
			{
				"file_name": "Gerüst",
				"source": "* FROM persons LIMIT 1"
			}
		]
	}
}'
```

```json
{
  "correct": false,
  "details": {
    "error": {
      "message": "Query is not a SELECT-statement",
      "queryId": "query2"
    },
    "payload": {
      "error": {
        "message": "Query is not a SELECT-statement",
        "queryId": "query2"
      },
      "query1": "SELECT * FROM customers LIMIT 1",
      "query2": "* FROM persons LIMIT 1",
      "request_type": "evaluate",
      "test_results": false
    },
    "runner_http_status": 200
  }
}
```

### /evaluate - solution schema invalid - query2 invalid 2

```json
curl --location --request POST 'localhost:5007/v1/evaluate' \
--header 'Content-Type: application/json' \
--data-raw '{
	"task": {
		"format_type": "sql",
		"evaluation": {
			"skip": false,
			"query1": {
				"source": "SELECT * FROM customers LIMIT 1"
			}
		}
	},
	"solution": {
		"current_source": 0,
		"sources": [
			{
				"file_name": "Gerüst",
				"source": "SELECT * FROM persons LIMIT 1"
			}
		]
	}
}'
```

```json
{
  "correct": false,
  "details": {
    "error": {
      "code": "SQLITE_ERROR",
      "errno": 1,
      "message": "SQLITE_ERROR: no such table: persons",
      "queryId": "query2"
    },
    "payload": {
      "error": {
        "code": "SQLITE_ERROR",
        "errno": 1,
        "message": "SQLITE_ERROR: no such table: persons",
        "queryId": "query2"
      },
      "query1": "SELECT * FROM customers LIMIT 1",
      "query2": "SELECT * FROM persons LIMIT 1",
      "queryResult1": [
        {
          "Address": "Av. Brigadeiro Faria Lima, 2170",
          "City": "São José dos Campos",
          "Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.",
          "Country": "Brazil",
          "CustomerId": 1,
          "Email": "luisg@embraer.com.br",
          "Fax": "+55 (12) 3923-5566",
          "FirstName": "Luís",
          "LastName": "Gonçalves",
          "Phone": "+55 (12) 3923-5555",
          "PostalCode": "12227-000",
          "State": "SP",
          "SupportRepId": 3
        }
      ],
      "request_type": "evaluate",
      "test_results": false
    },
    "runner_http_status": 200
  }
}
```

### /evaluate - solution schema invalid - queries valid - same results

```json
curl --location --request POST 'localhost:5007/v1/evaluate' \
--header 'Content-Type: application/json' \
--data-raw '{
	"task": {
		"format_type": "sql",
		"evaluation": {
			"skip": false,
			"query1": {
				"source": "SELECT * FROM customers LIMIT 1"
			}
		}
	},
	"solution": {
		"current_source": 0,
		"sources": [
			{
				"file_name": "Gerüst",
				"source": "SELECT * FROM customers LIMIT 1"
			}
		]
	}
}'
```

```json
{
  "correct": true,
  "details": {
    "payload": {
      "query1": "SELECT * FROM customers LIMIT 1",
      "query2": "SELECT * FROM customers LIMIT 1",
      "queryResult1": [
        {
          "Address": "Av. Brigadeiro Faria Lima, 2170",
          "City": "São José dos Campos",
          "Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.",
          "Country": "Brazil",
          "CustomerId": 1,
          "Email": "luisg@embraer.com.br",
          "Fax": "+55 (12) 3923-5566",
          "FirstName": "Luís",
          "LastName": "Gonçalves",
          "Phone": "+55 (12) 3923-5555",
          "PostalCode": "12227-000",
          "State": "SP",
          "SupportRepId": 3
        }
      ],
      "queryResult2": [
        {
          "Address": "Av. Brigadeiro Faria Lima, 2170",
          "City": "São José dos Campos",
          "Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.",
          "Country": "Brazil",
          "CustomerId": 1,
          "Email": "luisg@embraer.com.br",
          "Fax": "+55 (12) 3923-5566",
          "FirstName": "Luís",
          "LastName": "Gonçalves",
          "Phone": "+55 (12) 3923-5555",
          "PostalCode": "12227-000",
          "State": "SP",
          "SupportRepId": 3
        }
      ],
      "request_type": "evaluate",
      "test_results": true
    },
    "runner_http_status": 200
  }
}
```

### /evaluate - solution schema invalid - queries valid - different results

```json
curl --location --request POST 'localhost:5007/v1/evaluate' \
--header 'Content-Type: application/json' \
--data-raw '{
	"task": {
		"format_type": "sql",
		"evaluation": {
			"skip": false,
			"query1": {
				"source": "SELECT * FROM customers LIMIT 1"
			}
		}
	},
	"solution": {
		"current_source": 0,
		"sources": [
			{
				"file_name": "Gerüst",
				"source": "SELECT * FROM customers ORDER BY lastName LIMIT 1"
			}
		]
	}
}'
```

```json
{
  "correct": false,
  "details": {
    "payload": {
      "query1": "SELECT * FROM customers LIMIT 1",
      "query2": "SELECT * FROM customers ORDER BY lastName LIMIT 1",
      "queryResult1": [
        {
          "Address": "Av. Brigadeiro Faria Lima, 2170",
          "City": "São José dos Campos",
          "Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.",
          "Country": "Brazil",
          "CustomerId": 1,
          "Email": "luisg@embraer.com.br",
          "Fax": "+55 (12) 3923-5566",
          "FirstName": "Luís",
          "LastName": "Gonçalves",
          "Phone": "+55 (12) 3923-5555",
          "PostalCode": "12227-000",
          "State": "SP",
          "SupportRepId": 3
        }
      ],
      "queryResult2": [
        {
          "Address": "Praça Pio X, 119",
          "City": "Rio de Janeiro",
          "Company": "Riotur",
          "Country": "Brazil",
          "CustomerId": 12,
          "Email": "roberto.almeida@riotur.gov.br",
          "Fax": "+55 (21) 2271-7070",
          "FirstName": "Roberto",
          "LastName": "Almeida",
          "Phone": "+55 (21) 2271-7000",
          "PostalCode": "20040-020",
          "State": "RJ",
          "SupportRepId": 3
        }
      ],
      "request_type": "evaluate",
      "test_results": false
    },
    "runner_http_status": 200
  }
}
```

# Testing

- Test format-sql only:

```
docker-compose run --rm -e OPENPATCH_MODE="testing" backend python3 -m unittest tests.api.v1.formats.test_sql
```

- Test everything:

```
docker-compose run --rm -e OPENPATCH_MODE="testing" backend flask test
```
