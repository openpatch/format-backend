import os

import httpretty
from flask_testing import TestCase
from openpatch_format import create_app


@httpretty.activate
class BaseTest(TestCase):
    def create_app(self):
        return create_app("testing")

    def setUp(self):
        # mock authentification serivce, every jwt token is valid
        base_url = os.getenv("OPENPATCH_AUTHENTIFICATION_SERVICE")
        url = "%s/v1/verify" % base_url
        httpretty.register_uri(httpretty.GET, url, body="{}")

