from tests.base_test import BaseTest


class TestRoot(BaseTest):
    def test_evaluate(self):
        response = self.client.post(
            "/v1/evaluate",
            json={
                "task": {
                    "format_type": "highlight",
                    "format_version": 1,
                    "name": "Test",
                },
                "solution": {},
            },
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json(), {"correct": True, "details": {}})

        response = self.client.post(
            "/v1/evaluate", json={"task": {"name": "Test"}, "solution": {}}
        )

        self.assertEqual(response.status_code, 400)
        self.assertIn("Missing format_type", response.get_json().get("details"))

    def test_evaluate_missing_solution(self):
        response = self.client.post(
            "/v1/evaluate",
            json={
                "task": {
                    "format_type": "choice",
                    "format_version": 1,
                    "name": "Test",
                    "evaluation": {"choices": {"0": True}},
                }
            },
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json(), {"correct": False})

    def test_evaluate_batch(self):
        response = self.client.post(
            "/v1/evaluate-batch",
            json={
                "tasks": [
                    {"format_type": "highlight", "format_version": 1, "name": "Test"},
                    {"format_type": "highlight", "format_version": 1, "name": "Test"},
                ],
                "solutions": {"0": {}, "1": {}},
            },
        )

        self.assertEqual(response.status_code, 200)
        results = response.get_json().get("results")
        for key, result in results.items():
            self.assertEqual(result.get("correct"), True)

    def test_validate(self):
        response = self.client.post(
            "/v1/validate", json={"format_type": "highlight", "version": 1}
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.get_json(),
            {"correct": True, "task": {"evaluation": {"skip": True}}},
        )
