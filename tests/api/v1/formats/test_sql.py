import os
import httpretty
import unittest
from openpatch_format.api.v1.formats.sql import Sql
from tests.api.v1.formats import TestFormat


class TestCode(unittest.TestCase):
    # it's not class TestCode(TestFormat, unittest.TestCase) because the tests in TestFormat would fail.
    # For format-sql the /validate endpoint already checks if the provided SQL statement is syntactically correct and can be executed on the DB.
    # The test in TestFormat expect "correct": true in case of a request with {"data": {}}. As the solution query is missing, the response contains "correct": false

    Format = Sql

    # tests for validate()

    def test_validate__task_schema_invalid(self):
        # data object is missing --> correct: False
        task = {
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers LIMIT 1"
                }
            }
        }

        result = self.Format.validate(task)

        self.assertEqual(
            result,
            {
                "correct": False,
                "details": {
                    "error": {
                        "data": [
                            "Missing data for required field."
                        ]
                    }
                }
            }
        )

    @httpretty.activate
    def test_validate__task_schema_valid_query1_missing(self):
        # query1 is missing --> details contain error // correct: False
        # body contains response that is coming from the main runner
        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"error": "query1 is missing in the request.","request_type": "validate"},"runner_http_status": 200}')

        task = {
            "data": {},
            "evaluation": {
                "skip": False
            }
        }
        result = self.Format.validate(task)

        self.assertEqual(
            result,
            {
                "correct": False,
                "details": {
                    "error": {
                        "evaluation": {
                            "query1": [
                                "Missing data for required field."
                            ]
                        }
                    }
                }
            }
        )

    @httpretty.activate
    def test_validate__task_schema_valid_query1_invalid_1(self):
        # query1 is invalid --> details contain error // correct: False

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"error": {"message": "Query is not a SELECT-statement","queryId": "query1"},"query1": " * FROM customers","request_type": "validate","test_results": false},"runner_http_status": 200}')

        task = {
            "data": {},
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": " * FROM customers"
                }
            }
        }
        result = self.Format.validate(task)

        self.assertEqual(
            result,
            {
                "correct": False,
                "details": {
                    "error": {
                        "message": "Query is not a SELECT-statement",
                        "queryId": "query1"
                    }
                },
                "task": {
                    "data": {},
                    "evaluation": {
                        "query1": {
                            "source": " * FROM customers",
                        },
                        "skip": False
                    }
                }
            }
        )

    @httpretty.activate
    def test_validate__task_schema_valid_query1_invalid_2(self):

        # query1 is invalid (unknown table) --> details contain error // correct: False

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"error": {"code": "SQLITE_ERROR","errno": 1,"message": "SQLITE_ERROR: no such table: person","queryId": "query1"},"query1": "SELECT * FROM person","request_type": "validate","test_results": false},"runner_http_status": 200}')

        task = {
            "data": {},
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM person"
                }
            }
        }
        result = self.Format.validate(task)

        self.assertEqual(
            result,
            {
                "correct": False,
                "details": {
                    "error": {
                        "code": "SQLITE_ERROR",
                        "errno": 1,
                        "message": "SQLITE_ERROR: no such table: person",
                        "queryId": "query1"
                    }
                },
                "task": {
                    "data": {},
                    "evaluation": {
                        "query1": {
                            "source": "SELECT * FROM person"
                        },
                        "skip": False
                    }
                }
            }
        )

    @httpretty.activate
    def test_validate__task_schema_valid_query1_valid(self):
        # query1 is valid
        # task schema valid --> correct: True

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"query1": "SELECT * FROM customers LIMIT 1","queryResult1": [{"Address": "Av. Brigadeiro Faria Lima, 2170","City": "São José dos Campos","Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.","Country": "Brazil","CustomerId": 1,"Email": "luisg@embraer.com.br","Fax": "+55 (12) 3923-5566","FirstName": "Luís","LastName": "Gonçalves","Phone": "+55 (12) 3923-5555","PostalCode": "12227-000","State": "SP","SupportRepId": 3}],"request_type": "validate","test_results": true},"runner_http_status": 200}')

        task = {
            "data": {},
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers"
                }
            }
        }
        result = self.Format.validate(task)

        self.assertEqual(
            result,
            {
                "correct": True,
                "task": {
                    "data": {},
                    "evaluation": {
                        "query1": {
                            "source": "SELECT * FROM customers"
                        },
                        "skip": False
                    }
                }
            }
        )

    # tests for validate_task_schema()
    def test_validate_task_schema__invalid(self):
        # data object is missing --> correct: False
        task = {
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers LIMIT 1"
                }
            }
        }

        result = self.Format.validate_task_schema(task)

        self.assertEqual(
            result,
            {
                "correct": False,
                "error": {
                    "data": [
                        "Missing data for required field."
                    ]
                }
            }
        )

    def test_validate_task_schema__valid(self):
        # task schema is valid --> correct: True
        task = {
            "data": {},
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers LIMIT 1"
                }
            }
        }

        result = self.Format.validate_task_schema(task)

        self.assertEqual(
            result,
            {
                "correct": True
            }
        )

    # tests for validate_query()
    @httpretty.activate
    def test_validate_query__invalid(self):
        # query1 is invalid (unknown table) --> correct: False

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"error": {"code": "SQLITE_ERROR","errno": 1,"message": "SQLITE_ERROR: no such table: person","queryId": "query1"},"query1": "SELECT * FROM person","request_type": "validate","test_results": false},"runner_http_status": 200}')

        task = {
            "data": {},
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM person"
                }
            }
        }

        result = self.Format.validate_query(task)

        self.assertEqual(
            result,
            {
                "correct": False,
                "error": {
                    "code": "SQLITE_ERROR",
                    "errno": 1,
                    "message": "SQLITE_ERROR: no such table: person",
                    "queryId": "query1"
                }
            }
        )

    @httpretty.activate
    def test_validate_query__valid(self):
        # query1 is valid --> correct: True

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"query1": "SELECT * FROM customers LIMIT 1","queryResult1": [{"Address": "Av. Brigadeiro Faria Lima, 2170","City": "São José dos Campos","Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.","Country": "Brazil","CustomerId": 1,"Email": "luisg@embraer.com.br","Fax": "+55 (12) 3923-5566","FirstName": "Luís","LastName": "Gonçalves","Phone": "+55 (12) 3923-5555","PostalCode": "12227-000","State": "SP","SupportRepId": 3}],"request_type": "validate","test_results": true},"runner_http_status": 200}')

        task = {
            "data": {},
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers LIMIT 1"
                }
            }
        }

        result = self.Format.validate_query(task)

        self.assertEqual(
            result,
            {
                "correct": True
            }
        )

    # tests for evaluate()
    def test_evaluate__solution_schema_invalid(self):
        # solution schema is invalid (no solution query) --> correct: False
        task = {
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM person LIMIT 1"
                }
            }
        }
        evaluation = task.get("evaluation")
        solution = {}
        data = {}

        result = self.Format.evaluate(evaluation, solution, data)

        self.assertEqual(
            result,
            {
                "correct": False,
                "details": {
                    "error": {
                        "current_source": [
                            "Missing data for required field."
                        ],
                        "sources": [
                            "Missing data for required field."
                        ]
                    }
                }
            }
        )

    @httpretty.activate
    def test_evaluate__solution_schema_valid_query2_invalid_1(self):

        # solution schema is valid
        # query2 is invalid --> correct: False

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"error": {"message": "Query is not a SELECT-statement","queryId": "query2"},"query1": "SELECT * FROM customers LIMIT 1","query2": " * FROM persons LIMIT 1","request_type": "evaluate","test_results": false},"runner_http_status": 200}')

        task = {
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers LIMIT 1"
                }
            }
        }
        evaluation = task.get("evaluation")
        solution = {
            'current_source': 0,
            'sources': [
                {
                    'file_name': 'Gerüst',
                    'source': ' * FROM persons LIMIT 1'
                }
            ]
        }
        data = {}

        result = self.Format.evaluate(evaluation, solution, data)

        self.assertEqual(
            result,
            {
                "correct": False,
                "details": {
                    "error": {
                        "message": "Query is not a SELECT-statement",
                        "queryId": "query2"
                    },
                    "payload": {
                        "error": {
                            "message": "Query is not a SELECT-statement",
                            "queryId": "query2"
                        },
                        "query1": "SELECT * FROM customers LIMIT 1",
                        "query2": " * FROM persons LIMIT 1",
                        "request_type": "evaluate",
                        "test_results": False
                    },
                    "runner_http_status": 200
                }
            }
        )

    @httpretty.activate
    def test_evaluate__solution_schema_valid_query2_invalid_2(self):
        # solution schema is valid
        # query2 is invalid --> correct: False

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"error": {"code": "SQLITE_ERROR","errno": 1,"message": "SQLITE_ERROR: no such table: persons","queryId": "query2"},"query1": "SELECT * FROM customers LIMIT 1","query2": "SELECT * FROM persons LIMIT 1","queryResult1": [{"Address": "Av. Brigadeiro Faria Lima, 2170","City": "São José dos Campos","Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.","Country": "Brazil","CustomerId": 1,"Email": "luisg@embraer.com.br","Fax": "+55 (12) 3923-5566","FirstName": "Luís","LastName": "Gonçalves","Phone": "+55 (12) 3923-5555","PostalCode": "12227-000","State": "SP","SupportRepId": 3}],"request_type": "evaluate","test_results": false},"runner_http_status": 200}')

        task = {
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers LIMIT 1"
                }
            }
        }
        evaluation = task.get("evaluation")
        solution = {
            'current_source': 0,
            'sources': [
                {
                    'file_name': 'Gerüst',
                    'source': 'SELECT * FROM persons LIMIT 1'
                }
            ]
        }
        data = {}

        result = self.Format.evaluate(evaluation, solution, data)

        self.assertEqual(
            result,
            {
                "correct": False,
                "details": {
                    "error": {
                        "code": "SQLITE_ERROR",
                        "errno": 1,
                        "message": "SQLITE_ERROR: no such table: persons",
                        "queryId": "query2"
                    },
                    "payload": {
                        "error": {
                            "code": "SQLITE_ERROR",
                            "errno": 1,
                            "message": "SQLITE_ERROR: no such table: persons",
                            "queryId": "query2"
                        },
                        "query1": "SELECT * FROM customers LIMIT 1",
                        "query2": "SELECT * FROM persons LIMIT 1",
                        "queryResult1": [
                            {
                                "Address": "Av. Brigadeiro Faria Lima, 2170",
                                "City": "São José dos Campos",
                                "Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.",
                                "Country": "Brazil",
                                "CustomerId": 1,
                                "Email": "luisg@embraer.com.br",
                                "Fax": "+55 (12) 3923-5566",
                                "FirstName": "Luís",
                                "LastName": "Gonçalves",
                                "Phone": "+55 (12) 3923-5555",
                                "PostalCode": "12227-000",
                                "State": "SP",
                                "SupportRepId": 3
                            }
                        ],
                        "request_type": "evaluate",
                        "test_results": False
                    },
                    "runner_http_status": 200
                }
            }
        )

    @httpretty.activate
    def test_evaluate__solution_schema_valid_queries_valid_same_results(self):
        # solution schema is valid
        # query2 is valid and results are identical --> correct: True

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"query1": "SELECT * FROM customers LIMIT 1","query2": "SELECT * FROM customers LIMIT 1","queryResult1": [{"Address": "Av. Brigadeiro Faria Lima, 2170","City": "São José dos Campos","Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.","Country": "Brazil","CustomerId": 1,"Email": "luisg@embraer.com.br","Fax": "+55 (12) 3923-5566","FirstName": "Luís","LastName": "Gonçalves","Phone": "+55 (12) 3923-5555","PostalCode": "12227-000","State": "SP","SupportRepId": 3}],"queryResult2": [{"Address": "Av. Brigadeiro Faria Lima, 2170","City": "São José dos Campos","Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.","Country": "Brazil","CustomerId": 1,"Email": "luisg@embraer.com.br","Fax": "+55 (12) 3923-5566","FirstName": "Luís","LastName": "Gonçalves","Phone": "+55 (12) 3923-5555","PostalCode": "12227-000","State": "SP","SupportRepId": 3}],"request_type": "evaluate","test_results": true},"runner_http_status": 200}')

        task = {
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers LIMIT 1"
                }
            }
        }
        evaluation = task.get("evaluation")
        solution = {
            'current_source': 0,
            'sources': [
                {
                    'file_name': 'Gerüst',
                    'source': 'SELECT * FROM customers LIMIT 1'
                }
            ]
        }
        data = {}

        result = self.Format.evaluate(evaluation, solution, data)

        self.assertEqual(
            result,
            {
                "correct": True,
                "details": {
                    "payload": {
                        "query1": "SELECT * FROM customers LIMIT 1",
                        "query2": "SELECT * FROM customers LIMIT 1",
                        "queryResult1": [
                            {
                                "Address": "Av. Brigadeiro Faria Lima, 2170",
                                "City": "São José dos Campos",
                                "Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.",
                                "Country": "Brazil",
                                "CustomerId": 1,
                                "Email": "luisg@embraer.com.br",
                                "Fax": "+55 (12) 3923-5566",
                                "FirstName": "Luís",
                                "LastName": "Gonçalves",
                                "Phone": "+55 (12) 3923-5555",
                                "PostalCode": "12227-000",
                                "State": "SP",
                                "SupportRepId": 3
                            }
                        ],
                        "queryResult2": [
                            {
                                "Address": "Av. Brigadeiro Faria Lima, 2170",
                                "City": "São José dos Campos",
                                "Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.",
                                "Country": "Brazil",
                                "CustomerId": 1,
                                "Email": "luisg@embraer.com.br",
                                "Fax": "+55 (12) 3923-5566",
                                "FirstName": "Luís",
                                "LastName": "Gonçalves",
                                "Phone": "+55 (12) 3923-5555",
                                "PostalCode": "12227-000",
                                "State": "SP",
                                "SupportRepId": 3
                            }
                        ],
                        "request_type": "evaluate",
                        "test_results": True
                    },
                    "runner_http_status": 200
                }
            }

        )

    @httpretty.activate
    def test_evaluate__solution_schema_valid_queries_valid_different_results(self):
        # solution schema is valid
        # query2 is valid and results are different --> correct: False

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"query1": "SELECT * FROM customers LIMIT 1","query2": "SELECT * FROM customers ORDER BY lastName LIMIT 1","queryResult1": [{"Address": "Av. Brigadeiro Faria Lima, 2170","City": "São José dos Campos","Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.","Country": "Brazil","CustomerId": 1,"Email": "luisg@embraer.com.br","Fax": "+55 (12) 3923-5566","FirstName": "Luís","LastName": "Gonçalves","Phone": "+55 (12) 3923-5555","PostalCode": "12227-000","State": "SP","SupportRepId": 3}],"queryResult2": [{"Address": "Praça Pio X, 119","City": "Rio de Janeiro","Company": "Riotur","Country": "Brazil","CustomerId": 12,"Email": "roberto.almeida@riotur.gov.br","Fax": "+55 (21) 2271-7070","FirstName": "Roberto","LastName": "Almeida","Phone": "+55 (21) 2271-7000","PostalCode": "20040-020","State": "RJ","SupportRepId": 3}],"request_type": "evaluate","test_results": false},"runner_http_status": 200}')

        task = {
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers LIMIT 1"
                }
            }
        }
        evaluation = task.get("evaluation")
        solution = {
            'current_source': 0,
            'sources': [
                {
                    'file_name': 'Gerüst',
                    'source': 'SELECT * FROM customers ORDER BY lastName LIMIT 1'
                }
            ]
        }
        data = {}

        result = self.Format.evaluate(evaluation, solution, data)

        self.assertEqual(
            result,
            {
                "correct": False,
                "details": {
                    "payload": {
                        "query1": "SELECT * FROM customers LIMIT 1",
                        "query2": "SELECT * FROM customers ORDER BY lastName LIMIT 1",
                        "queryResult1": [
                            {
                                "Address": "Av. Brigadeiro Faria Lima, 2170",
                                "City": "São José dos Campos",
                                "Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.",
                                "Country": "Brazil",
                                "CustomerId": 1,
                                "Email": "luisg@embraer.com.br",
                                "Fax": "+55 (12) 3923-5566",
                                "FirstName": "Luís",
                                "LastName": "Gonçalves",
                                "Phone": "+55 (12) 3923-5555",
                                "PostalCode": "12227-000",
                                "State": "SP",
                                "SupportRepId": 3
                            }
                        ],
                        "queryResult2": [
                            {
                                "Address": "Praça Pio X, 119",
                                "City": "Rio de Janeiro",
                                "Company": "Riotur",
                                "Country": "Brazil",
                                "CustomerId": 12,
                                "Email": "roberto.almeida@riotur.gov.br",
                                "Fax": "+55 (21) 2271-7070",
                                "FirstName": "Roberto",
                                "LastName": "Almeida",
                                "Phone": "+55 (21) 2271-7000",
                                "PostalCode": "20040-020",
                                "State": "RJ",
                                "SupportRepId": 3
                            }
                        ],
                        "request_type": "evaluate",
                        "test_results": False
                    },
                    "runner_http_status": 200
                }
            }

        )
    # tests for validate_solution_schema()

    def test_validate_solution_schema__invalid(self):
        # query2 is missing in solution object --> correct: False
        solution = {}

        result = self.Format.validate_solution_schema(solution)

        self.assertEqual(
            result,
            {
                "correct": False,
                "error":
                    {
                        'current_source': [
                            'Missing data for required field.'
                        ],
                        'sources': [
                            'Missing data for required field.'
                        ]
                    }
            }
        )

    def test_validate_solution_schema__valid(self):
        # solution schema is valid --> correct: True
        solution = {
            'current_source': 0,
            'sources':
                [
                    {
                        'file_name': 'sth',
                        'source': 'SELECT * FROM customers LIMIT 1 '
                    }
                ]
        }

        result = self.Format.validate_solution_schema(solution)

        self.assertEqual(
            result,
            {
                "correct": True,
            }
        )

    # tests for evaluate_query()

    @httpretty.activate
    def test_evaluate_query__query2_invalid(self):
        # query2 is invalid --> correct: False

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"error": {"message": "Query is not a SELECT-statement","queryId": "query2"},"query1": "SELECT * FROM customers LIMIT 1","query2": " * FROM persons LIMIT 1","request_type": "evaluate","test_results": false},"runner_http_status": 200}')

        task = {
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers LIMIT 1"
                }
            }
        }

        evaluation = task.get("evaluation")
        solution = {
            "query2": " * FROM persons LIMIT 1"
        }
        data = {}
        result = self.Format.evaluate_query(evaluation, solution, data)

        self.assertEqual(
            result,
            {
                "correct": False,
                "details": {
                    "error": {
                        "message": "Query is not a SELECT-statement",
                        "queryId": "query2"
                    },
                    "payload": {
                        "error": {
                            "message": "Query is not a SELECT-statement",
                            "queryId": "query2"
                        },
                        "query1": "SELECT * FROM customers LIMIT 1",
                        "query2": " * FROM persons LIMIT 1",
                        "request_type": "evaluate",
                        "test_results": False
                    },
                    "runner_http_status": 200
                }
            }
        )

    @httpretty.activate
    def test_evaluate_query__queries_valid_same_results(self):

        # queries are valid
        # results are the same --> correct: True

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"query1": "SELECT * FROM customers LIMIT 1","query2": "SELECT * FROM customers LIMIT 1","queryResult1": [{"Address": "Av. Brigadeiro Faria Lima, 2170","City": "São José dos Campos","Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.","Country": "Brazil","CustomerId": 1,"Email": "luisg@embraer.com.br","Fax": "+55 (12) 3923-5566","FirstName": "Luís","LastName": "Gonçalves","Phone": "+55 (12) 3923-5555","PostalCode": "12227-000","State": "SP","SupportRepId": 3}],"queryResult2": [{"Address": "Av. Brigadeiro Faria Lima, 2170","City": "São José dos Campos","Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.","Country": "Brazil","CustomerId": 1,"Email": "luisg@embraer.com.br","Fax": "+55 (12) 3923-5566","FirstName": "Luís","LastName": "Gonçalves","Phone": "+55 (12) 3923-5555","PostalCode": "12227-000","State": "SP","SupportRepId": 3}],"request_type": "evaluate","test_results": true},"runner_http_status": 200}')

        task = {
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers LIMIT 1"
                }
            }
        }

        evaluation = task.get("evaluation")
        solution = {
            "query2": "SELECT * FROM customers LIMIT 1"
        }
        data = {}
        result = self.Format.evaluate_query(evaluation, solution, data)

        self.assertEqual(
            result,
            {
                "correct": True,
                "details": {
                    "payload": {
                        "query1": "SELECT * FROM customers LIMIT 1",
                        "query2": "SELECT * FROM customers LIMIT 1",
                        "queryResult1": [
                            {
                                "Address": "Av. Brigadeiro Faria Lima, 2170",
                                "City": "São José dos Campos",
                                "Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.",
                                "Country": "Brazil",
                                "CustomerId": 1,
                                "Email": "luisg@embraer.com.br",
                                "Fax": "+55 (12) 3923-5566",
                                "FirstName": "Luís",
                                "LastName": "Gonçalves",
                                "Phone": "+55 (12) 3923-5555",
                                "PostalCode": "12227-000",
                                "State": "SP",
                                "SupportRepId": 3
                            }
                        ],
                        "queryResult2": [
                            {
                                "Address": "Av. Brigadeiro Faria Lima, 2170",
                                "City": "São José dos Campos",
                                "Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.",
                                "Country": "Brazil",
                                "CustomerId": 1,
                                "Email": "luisg@embraer.com.br",
                                "Fax": "+55 (12) 3923-5566",
                                "FirstName": "Luís",
                                "LastName": "Gonçalves",
                                "Phone": "+55 (12) 3923-5555",
                                "PostalCode": "12227-000",
                                "State": "SP",
                                "SupportRepId": 3
                            }
                        ],
                        "request_type": "evaluate",
                        "test_results": True
                    },
                    "runner_http_status": 200
                }
            }
        )

    @httpretty.activate
    def test_evaluate_query__queries_valid_different_results(self):

        # queries are valid
        # results are different --> correct: False

        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(
            httpretty.POST, runner_url + "/api/v1/run", body='{"payload": {"query1": "SELECT * FROM customers LIMIT 1","query2": "SELECT * FROM customers WHERE CustomerId=10 LIMIT 1","queryResult1": [{"Address": "Av. Brigadeiro Faria Lima, 2170","City": "São José dos Campos","Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.","Country": "Brazil","CustomerId": 1,"Email": "luisg@embraer.com.br","Fax": "+55 (12) 3923-5566","FirstName": "Luís","LastName": "Gonçalves","Phone": "+55 (12) 3923-5555","PostalCode": "12227-000","State": "SP","SupportRepId": 3}],"queryResult2": [{"Address": "Rua Dr. Falcão Filho, 155","City": "São Paulo","Company": "Woodstock Discos","Country": "Brazil","CustomerId": 10,"Email": "eduardo@woodstock.com.br","Fax": "+55 (11) 3033-4564","FirstName": "Eduardo","LastName": "Martins","Phone": "+55 (11) 3033-5446","PostalCode": "01007-010","State": "SP","SupportRepId": 4}],"request_type": "evaluate","test_results": false},"runner_http_status": 200}')

        task = {
            "evaluation": {
                "skip": False,
                "query1": {
                    "source": "SELECT * FROM customers LIMIT 1"
                }
            }
        }

        evaluation = task.get("evaluation")
        solution = {
            "query2": "SELECT * FROM customers WHERE CustomerId=10 LIMIT 1"
        }
        data = {}
        result = self.Format.evaluate_query(evaluation, solution, data)

        self.assertEqual(
            result,
            {
                "correct": False,
                "details": {
                    "payload": {
                        "query1": "SELECT * FROM customers LIMIT 1",
                        "query2": "SELECT * FROM customers WHERE CustomerId=10 LIMIT 1",
                        "queryResult1": [
                            {
                                "Address": "Av. Brigadeiro Faria Lima, 2170",
                                "City": "São José dos Campos",
                                "Company": "Embraer - Empresa Brasileira de Aeronáutica S.A.",
                                "Country": "Brazil",
                                "CustomerId": 1,
                                "Email": "luisg@embraer.com.br",
                                "Fax": "+55 (12) 3923-5566",
                                "FirstName": "Luís",
                                "LastName": "Gonçalves",
                                "Phone": "+55 (12) 3923-5555",
                                "PostalCode": "12227-000",
                                "State": "SP",
                                "SupportRepId": 3
                            }
                        ],
                        "queryResult2": [
                            {
                                "Address": "Rua Dr. Falcão Filho, 155",
                                "City": "São Paulo",
                                "Company": "Woodstock Discos",
                                "Country": "Brazil",
                                "CustomerId": 10,
                                "Email": "eduardo@woodstock.com.br",
                                "Fax": "+55 (11) 3033-4564",
                                "FirstName": "Eduardo",
                                "LastName": "Martins",
                                "Phone": "+55 (11) 3033-5446",
                                "PostalCode": "01007-010",
                                "State": "SP",
                                "SupportRepId": 4
                            }
                        ],
                        "request_type": "evaluate",
                        "test_results": False
                    },
                    "runner_http_status": 200
                }
            }


        )

# tests for request_runner() are included in the other tests
