import unittest
from openpatch_format.api.v1.formats.ranking import Ranking
from tests.api.v1.formats import TestFormat


class TestRanking(TestFormat, unittest.TestCase):
    Format = Ranking
