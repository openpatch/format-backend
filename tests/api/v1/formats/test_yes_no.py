import unittest
from openpatch_format.api.v1.formats.yes_no import YesNo
from tests.api.v1.formats import TestFormat


class TestYesNo(TestFormat, unittest.TestCase):
    Format = YesNo

