import os
import json
import unittest
import httpretty
from openpatch_format.api.v1.formats.code import Code
from tests.api.v1.formats import TestFormat


class TestCode(TestFormat, unittest.TestCase):
    Format = Code

    def test_code_validate(self):
        task = {"data": {}, "evaluation": {}}
        result = self.Format.validate(task)

        self.assertEqual(
            result,
            {
                "correct": True,
                "task": {
                    "data": {},
                    "evaluation": {
                        "image": "registry.gitlab.com/openpatch/runner-java:v1.0.0",
                        "skip": True,
                    },
                },
            },
        )

    def test_make_java_runner_source(self):
        runner_source = self.Format.make_java_runner_source(
            "Hi You", "public class Show {}"
        )
        self.assertEqual(runner_source["fqcn"], "Show")

    def test_make_java_runner_source_in_package(self):
        runner_source = self.Format.make_java_runner_source(
            "Hi You", "package app.openpatch; public class Code {}"
        )
        self.assertEqual(runner_source["fqcn"], "app.openpatch.Code")

    def test_make_java_runner_source_nothing(self):
        runner_source = self.Format.make_java_runner_source("Hallo.java", "")
        self.assertEqual(runner_source["fqcn"], "Hallo")

    @httpretty.activate
    def test_evaluate_code_image(self):
        runner_url = os.getenv("OPENPATCH_RUNNER_SERVICE")
        httpretty.register_uri(httpretty.POST, runner_url + "/api/v1/run", body="{}")

        def test_image(image):
            solution = {
                "sources": [
                    {"file_name": "Haus.java", "source": "public class Haus {}"}
                ]
            }
            evaluation = {
                "image": image,
                "tests": [{"file_name": "TestHaus.java", "source": ""}],
            }
            result = self.Format.evaluate(evaluation, solution, None)
            self.assertTrue(result["correct"])

            payload = {
                "sources": [{"fqcn": "Haus", "code": "public class Haus {}"}],
                "tests": [{"fqcn": "TestHaus", "code": ""}],
            }

            last_request = json.loads(httpretty.last_request().body)
            self.assertEqual(last_request["payload"], payload)
            self.assertEqual(last_request["timeout"], 30)
            self.assertEqual(last_request["image"], image)

        for image in [
            "registry.gitlab.com/openpatch/runner-java:v0.1.0",
            "registry.gitlab.com/openpatch/runner-java:v1.0.0",
            "registry.gitlab.com/openpatch/runner-java:v1.1.0",
        ]:
            test_image(image)
