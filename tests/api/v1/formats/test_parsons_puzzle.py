import unittest
from openpatch_format.api.v1.formats.parsons_puzzle import ParsonsPuzzle
from tests.api.v1.formats import TestFormat


class TestParsonsPuzzle(TestFormat, unittest.TestCase):
    Format = ParsonsPuzzle

