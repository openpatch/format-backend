import unittest
from openpatch_format.api.v1.formats.display import Display
from tests.api.v1.formats import TestFormat


class TestDisplay(TestFormat, unittest.TestCase):
    Format = Display

