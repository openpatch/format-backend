import unittest
from openpatch_format.api.v1.formats.text import Text
from tests.api.v1.formats import TestFormat


class TestText(TestFormat, unittest.TestCase):
    Format = Text
