import unittest
from openpatch_format.api.v1.formats.highlight import Highlight
from tests.api.v1.formats import TestFormat

task = {
    "format_type": "highlight",
    "data": {
        "text": {
            "blocks": [
                {
                    "key": "dmcg5",
                    "text": "Hallo",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "8ih59",
                    "text": "",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "6tu7h",
                    "text": "Hallo und so weiter",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "dli9i",
                    "text": "",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "dta4h",
                    "text": "",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
            ],
            "entityMap": {},
        },
        "colors": ["#ff0000", "#0000ff"],
    },
    "evaluation": {
        "cutoff": 0.6,
        "highlighted_text": {
            "blocks": [
                {
                    "key": "dmcg5",
                    "text": "Hallo",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [
                        {"offset": 0, "length": 5, "style": "#ff0000"}
                    ],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "8ih59",
                    "text": "",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "6tu7h",
                    "text": "Hallo und so weiter",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [
                        {"offset": 13, "length": 6, "style": "#0000ff"}
                    ],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "dli9i",
                    "text": "",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "dta4h",
                    "text": "",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
            ],
            "entity_map": {},
        },
    },
}


class TestHighlight(TestFormat, unittest.TestCase):
    Format = Highlight

    def test_validate(self):
        result = self.Format.validate(task)
        self.assertTrue(result["correct"])

    def test_evaluate_perfect(self):
        solution = {
            "highlighted_text": {
                "blocks": [
                    {
                        "key": "dmcg5",
                        "text": "Hallo",
                        "type": "unstyled",
                        "depth": 0,
                        "inline_style_ranges": [
                            {"offset": 0, "length": 5, "style": "#ff0000"}
                        ],
                        "entity_ranges": [],
                        "data": {},
                    },
                    {
                        "key": "8ih59",
                        "text": "",
                        "type": "unstyled",
                        "depth": 0,
                        "inline_style_ranges": [],
                        "entity_ranges": [],
                        "data": {},
                    },
                    {
                        "key": "6tu7h",
                        "text": "Hallo und so weiter",
                        "type": "unstyled",
                        "depth": 0,
                        "inline_style_ranges": [
                            {"offset": 13, "length": 6, "style": "#0000ff"}
                        ],
                        "entity_ranges": [],
                        "data": {},
                    },
                    {
                        "key": "dli9i",
                        "text": "",
                        "type": "unstyled",
                        "depth": 0,
                        "inline_style_ranges": [],
                        "entity_ranges": [],
                        "data": {},
                    },
                    {
                        "key": "dta4h",
                        "text": "",
                        "type": "unstyled",
                        "depth": 0,
                        "inline_style_ranges": [],
                        "entity_ranges": [],
                        "data": {},
                    },
                ],
                "entity_map": {},
            }
        }

        result = self.Format.evaluate(task["evaluation"], solution, task["data"])
        self.assertTrue(result["correct"])
        self.assertEqual(result["details"]["#ff0000"]["kappa"], 1)
        self.assertEqual(result["details"]["#0000ff"]["kappa"], 1)

    def test_evaluate_incorrect(self):
        solution = {
            "highlighted_text": {
                "blocks": [
                    {
                        "key": "dmcg5",
                        "text": "Hallo",
                        "type": "unstyled",
                        "depth": 0,
                        "inline_style_ranges": [
                            {"offset": 0, "length": 5, "style": "#ff0000"}
                        ],
                        "entity_ranges": [],
                        "data": {},
                    },
                    {
                        "key": "8ih59",
                        "text": "",
                        "type": "unstyled",
                        "depth": 0,
                        "inline_style_ranges": [],
                        "entity_ranges": [],
                        "data": {},
                    },
                    {
                        "key": "6tu7h",
                        "text": "Hallo und so weiter",
                        "type": "unstyled",
                        "depth": 0,
                        "inline_style_ranges": [
                            {"offset": 1, "length": 6, "style": "#0000ff"}
                        ],
                        "entity_ranges": [],
                        "data": {},
                    },
                    {
                        "key": "dli9i",
                        "text": "",
                        "type": "unstyled",
                        "depth": 0,
                        "inline_style_ranges": [],
                        "entity_ranges": [],
                        "data": {},
                    },
                    {
                        "key": "dta4h",
                        "text": "",
                        "type": "unstyled",
                        "depth": 0,
                        "inline_style_ranges": [],
                        "entity_ranges": [],
                        "data": {},
                    },
                ],
                "entity_map": {},
            }
        }

        result = self.Format.evaluate(task["evaluation"], solution, task["data"])
        self.assertFalse(result["correct"])
        self.assertEqual(result["details"]["#ff0000"]["kappa"], 1)
        self.assertEqual(result["details"]["#0000ff"]["kappa"], -1 / 3)

    def test_evaluate_no_solution(self):
        result = self.Format.evaluate(task["evaluation"], None, task["data"])
        self.assertFalse(result["correct"])
        self.assertEqual(result["details"]["#ff0000"]["kappa"], 0)
        self.assertEqual(result["details"]["#0000ff"]["kappa"], 0)

