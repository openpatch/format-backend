import unittest
from openpatch_format.api.v1.formats.time import Time
from tests.api.v1.formats import TestFormat


class TestTime(TestFormat, unittest.TestCase):
    Format = Time

