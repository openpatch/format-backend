class TestFormat:
    def test_no_evaluation(self):
        task = {"data": {}}

        result = self.Format.validate(task)
        self.assertTrue(result["correct"])
        self.assertTrue(result["task"]["evaluation"]["skip"])

    def test_skip_is_not_removed(self):
        task = {"evaluation": {"skip": True}, "data": {}}
        result = self.Format.validate(task)

        self.assertTrue(result["correct"])
        self.assertTrue(result["task"]["evaluation"]["skip"])
