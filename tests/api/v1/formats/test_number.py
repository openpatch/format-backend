import unittest
from openpatch_format.api.v1.formats.number import Number
from tests.api.v1.formats import TestFormat


class TestNumber(TestFormat, unittest.TestCase):
    Format = Number

