import unittest
from openpatch_format.api.v1.formats.choice import Choice
from tests.api.v1.formats import TestFormat


class TestChoice(TestFormat, unittest.TestCase):
    Format = Choice

    def test_choice_validate_multiple_true(self):
        task = {
            "data": {"allow_multiple": True, "choices": [{}, {}, {}]},
            "evaluation": {"skip": False, "choices": {"0": True, "1": True}},
        }
        result = self.Format.validate(task)
        self.assertTrue(result["correct"])
        self.assertEqual(result["task"], task)

    def test_choice_validate_single_true(self):
        task = {
            "data": {"allow_multiple": False, "choices": [{}, {}, {}]},
            "evaluation": {"skip": False, "choices": {"0": True}},
        }
        result = self.Format.validate(task)
        self.assertTrue(result["correct"])
        self.assertEqual(result["task"], task)

    def test_choice_validate_mutiple_without_allow_multiple(self):
        task = {
            "data": {"allow_multiple": False, "choices": [{}, {}, {}]},
            "evaluation": {"skip": False, "choices": {"0": True, "1": True}},
        }
        result = self.Format.validate(task)
        self.assertFalse(result["correct"])
        self.assertIn("details", result)

    def test_choice_evaluate_true(self):
        evaluation = {"choices": {"0": True, "1": False}}
        solution = {"choices": {"0": True, "2": False}}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertTrue(result["correct"])

    def test_choice_evaluate_false(self):
        evaluation = {"choices": {"0": True, "1": False}}
        solution = {"choices": {"1": True, "2": False}}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])

    def test_choice_evaluate_solution_empty(self):
        evaluation = {"choices": {"0": True, "1": False}}
        solution = {}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])
