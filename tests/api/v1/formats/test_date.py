import unittest
from openpatch_format.api.v1.formats.date import Date
from tests.api.v1.formats import TestFormat


class TestDate(TestFormat, unittest.TestCase):
    Format = Date

    def test_validate(self):
        task = {
            "format_type": "date",
            "evaluation": {
                "value": "2020-01-25T22:28:25.566Z",
                "operator": "==",
                "min": "2020-01-20T22:28:25.566Z",
                "max": "2020-01-28T22:28:25.566Z",
            },
        }

        result = self.Format.validate(task)
        self.assertTrue(result["correct"])

    def test_validate_no_min(self):
        task = {
            "format_type": "date",
            "evaluation": {
                "value": "2020-01-25T22:28:25.566Z",
                "operator": "==",
                "max": "2020-01-28T22:28:25.566Z",
            },
        }

        result = self.Format.validate(task)
        self.assertTrue(result["correct"])

    def test_validate_no_max(self):
        task = {
            "format_type": "date",
            "evaluation": {
                "value": "2020-01-25T22:28:25.566Z",
                "operator": "==",
                "min": "2020-01-22T22:28:25.566Z",
            },
        }

        result = self.Format.validate(task)
        self.assertTrue(result["correct"])

    def test_validate_no_value(self):
        task = {
            "format_type": "date",
            "evaluation": {"operator": "==", "min": "2020-01-22T22:28:25.566Z"},
        }

        result = self.Format.validate(task)
        self.assertTrue(result["correct"])

    def test_validate_no_operator(self):
        task = {
            "format_type": "date",
            "evaluation": {"value": "2020-01-22T22:28:25.566Z"},
        }

        result = self.Format.validate(task)
        self.assertFalse(result["correct"])

    def test_validate_min_greater_max(self):
        task = {
            "format_type": "date",
            "evaluation": {
                "value": "2020-01-25T22:28:25.566Z",
                "operator": "==",
                "min": "2020-01-30T22:28:25.566Z",
                "max": "2020-01-28T22:28:25.566Z",
            },
        }

        result = self.Format.validate(task)
        self.assertFalse(result["correct"])

    def test_validate_value_not_between_min_max(self):
        task = {
            "format_type": "date",
            "evaluation": {
                "value": "2020-01-25T22:28:25.566Z",
                "operator": "==",
                "min": "2020-01-26T22:28:25.566Z",
                "max": "2020-01-28T22:28:25.566Z",
            },
        }

        result = self.Format.validate(task)
        self.assertFalse(result["correct"])

    def test_validate_malformed_date(self):
        task = {
            "format_type": "date",
            "evaluation": {
                "value": "2020-01-25T22:28:25.566Z",
                "operator": "==",
                "min": "2020-01-2:28:25.566Z",
            },
        }

        result = self.Format.validate(task)
        self.assertFalse(result["correct"])

    def test_evaluate_equals(self):
        evaluation = {
            "value": "2020-01-25T22:28:25.566Z",
            "operator": "==",
            "min": "2020-01-20T22:28:25.566Z",
            "max": "2020-01-28T22:28:25.566Z",
        }
        solution = {"value": "2020-01-25T22:28:25.566Z"}

        result = self.Format.evaluate(evaluation, solution, None)
        self.assertTrue(result["correct"])

        solution = {"value": "2020-01-24T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])

    def test_evaluate_leq(self):
        evaluation = {
            "value": "2020-01-25T22:28:25.566Z",
            "operator": "<=",
            "min": "2020-01-20T22:28:25.566Z",
            "max": "2020-01-28T22:28:25.566Z",
        }
        # equals
        solution = {"value": "2020-01-25T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertTrue(result["correct"])

        # smaller
        solution = {"value": "2020-01-24T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertTrue(result["correct"])

        # greater
        solution = {"value": "2020-01-26T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])

    def test_evaluate_less(self):
        evaluation = {
            "value": "2020-01-25T22:28:25.566Z",
            "operator": "<",
            "min": "2020-01-20T22:28:25.566Z",
            "max": "2020-01-28T22:28:25.566Z",
        }
        # equals
        solution = {"value": "2020-01-25T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])

        # smaller
        solution = {"value": "2020-01-24T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertTrue(result["correct"])

        # greater
        solution = {"value": "2020-01-26T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])

    def test_evaluate_geq(self):
        evaluation = {
            "value": "2020-01-25T22:28:25.566Z",
            "operator": ">=",
            "min": "2020-01-20T22:28:25.566Z",
            "max": "2020-01-28T22:28:25.566Z",
        }
        # equals
        solution = {"value": "2020-01-25T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertTrue(result["correct"])

        # smaller
        solution = {"value": "2020-01-24T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])

        # greater
        solution = {"value": "2020-01-26T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertTrue(result["correct"])

    def test_evaluate_greater(self):
        evaluation = {
            "value": "2020-01-25T22:28:25.566Z",
            "operator": ">",
            "min": "2020-01-20T22:28:25.566Z",
            "max": "2020-01-28T22:28:25.566Z",
        }
        # equals
        solution = {"value": "2020-01-25T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])

        # smaller
        solution = {"value": "2020-01-24T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])

        # greater
        solution = {"value": "2020-01-26T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertTrue(result["correct"])

    def test_evaluate_less_than_min(self):
        evaluation = {
            "min": "2020-01-20T22:28:25.566Z",
            "max": "2020-01-28T22:28:25.566Z",
        }

        solution = {"value": "2020-01-19T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])

    def test_evaluate_greater_than_max(self):
        evaluation = {
            "min": "2020-01-20T22:28:25.566Z",
            "max": "2020-01-28T22:28:25.566Z",
        }

        solution = {"value": "2020-01-30T22:28:25.566Z"}
        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])

    def test_evaluate_no_solution(self):
        evaluation = {
            "min": "2020-01-20T22:28:25.566Z",
            "max": "2020-01-28T22:28:25.566Z",
        }

        result = self.Format.evaluate(evaluation, None, None)
        self.assertFalse(result["correct"])

