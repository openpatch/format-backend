import unittest
from openpatch_format.api.v1.formats.hotspot import Hotspot
from tests.api.v1.formats import TestFormat


class TestHotspot(TestFormat, unittest.TestCase):
    Format = Hotspot

