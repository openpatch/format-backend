import unittest
from openpatch_format.api.v1.formats.memorize import Memorize
from tests.api.v1.formats import TestFormat


class TestMemorize(TestFormat, unittest.TestCase):
    Format = Memorize

