import unittest
from openpatch_format.api.v1.formats.block_tapping import BlockTapping
from tests.api.v1.formats import TestFormat


class TestBlockTapping(TestFormat, unittest.TestCase):
    Format = BlockTapping

