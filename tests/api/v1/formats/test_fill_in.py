import unittest
from openpatch_format.api.v1.formats.fill_in import FillIn
from tests.api.v1.formats import TestFormat


class TestFillIn(TestFormat, unittest.TestCase):
    Format = FillIn

    def test_validate_regex(self):
        task = {
            "format_type": "fill-in",
            "data": {"text": {}},
            "evaluation": {"inputs": [{"regex": "d+"}, {"regex": "e+"}]},
        }

        result = self.Format.validate(task)
        self.assertTrue(result["correct"])

    def test_validate_malformed_regex(self):
        task = {
            "format_type": "fill-in",
            "data": {"text": {}},
            "evaluation": {"inputs": [{"regex": "d+("}, {"regex": "e+"}]},
        }

        result = self.Format.validate(task)
        self.assertFalse(result["correct"])

    def test_evaluate_correct(self):
        evaluation = {"inputs": [{"regex": "d+"}, {"regex": "e+"}]}

        solution = {"values": {"0": "ddd", "1": "eee"}}

        result = self.Format.evaluate(evaluation, solution, None)
        self.assertTrue(result["correct"])

    def test_evaluate_incorrect(self):
        evaluation = {"inputs": [{"regex": "d+"}, {"regex": "e+"}]}

        solution = {"values": {"0": "ddd", "1": "ccc"}}

        result = self.Format.evaluate(evaluation, solution, None)
        self.assertFalse(result["correct"])

    def test_evaluate_no_solution(self):
        evaluation = {"inputs": [{"regex": "d+"}, {"regex": "e+"}]}

        result = self.Format.evaluate(evaluation, None, None)
        self.assertFalse(result["correct"])
