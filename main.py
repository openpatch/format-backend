import os
import sys
import unittest

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from coverage import Coverage
from openpatch_format import create_app

environment = os.getenv("OPENPATCH_MODE", "development")
app = create_app(environment)

sentry_dsn = os.getenv("SENTRY_DSN", None)

if sentry_dsn:
    sentry_sdk.init(
        dsn=sentry_dsn, integrations=[FlaskIntegration()], environment=environment
    )


@app.cli.command()
def test():
    tests = unittest.TestLoader().discover("tests", pattern="test*.py")
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    sys.exit(1)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
