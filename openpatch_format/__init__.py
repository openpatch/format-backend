from flask import Flask, jsonify
from flask_cors import cross_origin
from instance.config import app_config
from openpatch_format.api.v1 import api as api_v1
from openpatch_core.errors import error_manager


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    error_manager.init_app(app)

    # register api endpoints
    app.register_blueprint(api_v1, url_prefix="/v1")

    @app.route("/healthcheck", methods=["GET"])
    @cross_origin()
    def healthcheck():
        return jsonify({"msg": "ok"}), 200

    return app
