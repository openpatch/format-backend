from marshmallow import fields, Schema, validate, EXCLUDE


def raw_to_text(raw):
    raw = RawDraftContent.load(raw)
    text = ""
    for block in raw.get("blocks", []):
        text = get_text_from_block(block, text)
    return text


def get_text_from_block(block, text):
    text += block.get("text", "") + " "
    for b in block.get("children", []):
        text = get_text_from_block(b, text)
    return text


class InlineStyleRange(Schema):
    class Meta:
        unknown = EXCLUDE

    style = fields.Str()
    offset = fields.Int()
    length = fields.Int()


class RawDraftContentBlock(Schema):
    class Meta:
        unknown = EXCLUDE

    key = fields.Str()
    type = fields.Str(
        validate=validate.OneOf(
            [
                "unstyled",
                "paragraph",
                "header-one",
                "header-two",
                "header-three",
                "header-four",
                "header-five",
                "header-six",
                "unordered-list-item",
                "ordered-list-item",
                "blockquote",
                "code-block",
                "atomic",
            ]
        )
    )
    text = fields.Str()
    depth = fields.Int()
    inline_style_ranges = fields.Nested(InlineStyleRange, many=True)
    data = fields.Dict()
    children = fields.Nested(lambda: RawDraftContentBlock(), many=True)


class RawDraftEntity(Schema):
    class Meta:
        unknown = EXCLUDE

    type = fields.Str()
    mutability = fields.Str(
        validate=validate.OneOf(["MUTABLE", "IMMUTABLE", "SEGMENTED"])
    )
    data = fields.Dict(keys=fields.Str())


class RawDraftContent(Schema):
    class Meta:
        unknown = EXCLUDE

    blocks = fields.Nested(RawDraftContentBlock, many=True)
    entity_map = fields.Dict(keys=fields.Str(), values=fields.Nested(RawDraftEntity))
