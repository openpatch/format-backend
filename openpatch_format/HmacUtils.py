import hashlib
import hmac

import requests

from instance.config import get_config


def sign_request(request):
    """Signs a requests.request-object by adding a HMAC-header, which is created over the `.data`-attribute.

    The `.data`-attribute shall be encoded with `utf-8`, i.e. it must not contain binary data, otherwise it can not
    be signed. After the 'HMAC'-header has been added, you can use the following pattern, to send the request:
    `response = requests.Session().send(signed_prep_req)`.

    Alternatively, you can use `HmacUtils.sign_and_send()`.

    Attention: If no OPENPATCH_HMAC_SECRET-environment variable is set, a log entry is created and no header is added.

    :param request: An unprepared `requests.request` object.
    :return: A prepared `requests.request` object, which has a HMAC-header.
    """
    prep_req = request.prepare()

    if get_config().OPENPATCH_HMAC_SECRET is None:
        return prep_req
    else:
        prep_req.headers["HMAC"] = hmac.new(
            bytes(get_config().OPENPATCH_HMAC_SECRET, "utf-8"),
            bytes(str(prep_req.body), "utf-8"),
            hashlib.sha512,
        ).hexdigest()
        return prep_req


def sign_and_send(request, timeout=30):
    """Signs and sends a prepared requests.request-object.

    This is a wrapper to sign_request() in combination with opening a session and sending the message in one line.

    :param request: A  request, which will be signed and sent.
    :param timeout: (Optional) (Int) The timeout, after which a sent request timeouts. Defaults to 20.
    :return (requests.Response) Response
    :raises requests.exceptions.ReadTimeout: when a runner is killed.
    """

    return requests.Session().send(sign_request(request), timeout=timeout)
