from openpatch_core.errors import error_manager as em


def invalid_json(errors):
    """
    @apiDefine errors_invalid_json
    @apiError (ErrorCode 1) {String} message InvalidJSON
    @apiError (ErrorCode 1) {Integer} status_code 400
    @apiError (ErrorCode 1) {Integer} code 1
    """
    return em.make_json_error(
        400, message="Invalid JSON in request body", details=errors, code=1
    )


def no_json():
    """
    @apiDefine errors_no_json
    @apiError (ErrorCode 2) {String} message NoJSON
    @apiError (ErrorCode 2) {Integer} status_code 400
    @apiError (ErrorCode 2) {Integer} code 2
    """
    return em.make_json_error(400, message="No JSON in request body", code=2)


def resource_not_found():
    """
    @apiDefine errors_resource_not_found
    @apiError (ErrorCode 110) {String} message ResourceNotFound
    @apiError (ErrorCode 110) {Integer} status_code 404
    @apiError (ErrorCode 110) {Integer} code 110
    """
    return em.make_json_error(404, message="Resource not found", code=110)


def service_not_available():
    """
    @apiDefine errors_service_not_available
    @apiError (ErrorCode 503) {String} message ServiceNotAvailable
    @apiError (ErrorCode 503) {Integer} status_code 503
    @apiError (ErrorCode 503) {Integer} code 111
    """
    return em.make_json_error(503, message="Service not available", code=111)
