from flask import jsonify, request
from openpatch_format.api.v1 import api, errors
from openpatch_format.api.v1.formats.code import Code
from openpatch_format.api.v1.formats.display import Display
from openpatch_format.api.v1.formats.fill_in import FillIn
from openpatch_format.api.v1.formats.ranking import Ranking
from openpatch_format.api.v1.formats.text import Text
from openpatch_format.api.v1.formats.choice import Choice
from openpatch_format.api.v1.formats.number import Number
from openpatch_format.api.v1.formats.highlight import Highlight
from openpatch_format.api.v1.formats.date import Date
from openpatch_format.api.v1.formats.time import Time
from openpatch_format.api.v1.formats.hotspot import Hotspot
from openpatch_format.api.v1.formats.block_tapping import BlockTapping
from openpatch_format.api.v1.formats.yes_no import YesNo
from openpatch_format.api.v1.formats.sql import Sql

base_url = "/"


def get_format(format_type):
    Format = Display
    if format_type == "fill-in":
        Format = FillIn
    elif format_type == "code":
        Format = Code
    elif format_type == "text":
        Format = Text
    elif format_type == "highlight":
        Format = Highlight
    elif format_type == "number" or format_type == "scale":
        Format = Number
    elif format_type == "ranking":
        Format = Ranking
    elif format_type == "hotspot":
        Format = Hotspot
    elif format_type == "choice" or format_type == "dropdown":
        Format = Choice
    elif format_type == "date":
        Format = Date
    elif format_type == "time":
        Format = Time
    elif format_type == "block-tapping":
        Format = BlockTapping
    elif format_type == "yes-no":
        Format = YesNo
    elif format_type == "sql":
        Format = Sql
    return Format


@api.route(base_url + "/evaluate-batch", methods=["POST"])
def evaluate_batch():
    """
    @api {post} /v1/evaluate-batch Evaluate a batch of solutions
    @apiVersion 1.0.0
    @apiName EvaluateBatch
    @apiGroup Format
    @apiDescription This endpoint is used to evaluate more than one solution at once.

    @apiParam {Object[]} tasks
    @apiParam {String} tasks.format_type The format type of a task
    @apiParam {Object} tasks.data The data to render the task
    @apiParam {Object} tasks.evaluation The data to evaluate a solution
    @apiParam {Object[]} solutions An array of solution for each task

    @apiParam {Object[]} results Containes details about each task
    @apiParam {Boolean} correct Indicates if all tasks were correct

    @apiUse errors_invalid_json
    @apiUse errors_no_json
    @apiUse errors_service_not_available
    """
    evaluate_json = request.get_json()

    tasks = evaluate_json.get("tasks", [])
    solutions = evaluate_json.get("solutions", {})

    results = {}
    correct = True
    for i in range(len(tasks)):
        task = tasks[i]
        solution = solutions.get(str(i), {})
        format_type = task.get("format_type")

        Format = get_format(format_type)
        evaluation = task.get("evaluation", {"skip": True})
        if not evaluation or evaluation.get("skip"):
            results[i] = {"skipped": True, "correct": True}
            continue

        result = {"correct": False, "solution": solution}
        if solution is not None:
            result = Format.evaluate(evaluation, solution, task.get("data"))

        correct = correct and result.get("correct", False)
        results[i] = result

    return jsonify({"correct": correct, "results": results})


@api.route(base_url + "/evaluate", methods=["POST"])
def evaluate():
    """
    @api {post} /v1/evaluate Evaluate a solution
    @apiVersion 1.0.0
    @apiName Evaluate
    @apiGroup Format
    @apiDescription This endpoint is used to evaluate one solution

    @apiParam {Object} task
    @apiParam {String} task.format_type
    @apiParam {Object} task.evaluation
    @apiParam {Object} solution

    @apiUse errors_invalid_json
    @apiUse errors_no_json
    @apiUse errors_service_not_available
    """

    evaluate_json = request.get_json()

    if not evaluate_json:
        return errors.no_json()

    task_json = evaluate_json.get("task")
    if not task_json:
        return errors.invalid_json(["Missing task"])

    format_type = task_json.get("format_type")
    if not format_type:
        return errors.invalid_json(["Missing format_type"])

    solution_json = evaluate_json.get("solution", {})

    Format = get_format(format_type)

    evaluation_json = task_json.get("evaluation", {})
    data_json = task_json.get("data", {})
    result = Format.evaluate(evaluation_json, solution_json, data_json)

    return jsonify(result)


@api.route(base_url + "/validate", methods=["POST"])
def validate():
    """
    @api {post} /v1/validate Validate a task
    @apiVersion 1.0.0
    @apiName Validate
    @apiGroup Format
    @apiDescription This endpoint is used to validate a task. It also
    cleans the received object by deleting unnecessary properties.

    @apiParam {String} format_type The type of the format
    @apiParam {Object} task The task
    @apiParam {Object} task.data The data to render the task
    @apiParam {Object} task.evaluation The data to evaluate a solution

    @apiUse errors_invalid_json
    @apiUse errors_no_json
    """
    validate_json = request.get_json()

    if not validate_json:
        return errors.no_json()

    if not validate_json.get("format_type"):
        return errors.invalid_json(["Missing format type"])

    format_type = validate_json.get("format_type")

    Format = get_format(format_type)

    result = Format.validate(validate_json)

    return jsonify(result)
