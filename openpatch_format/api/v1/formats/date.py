from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.api.v1.formats import Format


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    value = fields.DateTime()
    operator = fields.Str(validate=validate.OneOf(["==", ">=", "<=", ">", "<"]))

    min = fields.DateTime(allow_none=True)
    max = fields.DateTime(allow_none=True)

    @validates_schema
    def validate_min_max(self, data, **kwargs):
        value = data.get("value")
        operator = data.get("operator")
        min = data.get("min")
        max = data.get("max")
        if min is not None and max is not None and min > max:
            raise ValidationError("min can not be greater than max", "min")

        if value is not None:
            if min is not None and value < min:
                raise ValidationError("value can not be smaller than min", "value")
            if max is not None and value > max:
                raise ValidationError("value can not be greater than max", "value")
            if operator is None:
                raise ValidationError("operator is needed", "operator")


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    value = fields.DateTime()


class Date(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        operator = evaluation.get("operator")

        evaluation = EvaluationSchema().load(evaluation)
        try:
            solution = SolutionSchema().load(solution)
        except ValidationError:
            solution = {}

        correct = True
        sol_value = solution.get("value")
        eval_value = evaluation.get("value")
        min = evaluation.get("min", None)
        max = evaluation.get("max", None)

        if min:
            correct = correct and sol_value and sol_value > min
        if max:
            correct = correct and sol_value and sol_value < max

        if operator == "==":
            correct = correct and sol_value and sol_value == eval_value
        elif operator == ">=":
            correct = correct and sol_value and sol_value >= eval_value
        elif operator == "<=":
            correct = correct and sol_value and sol_value <= eval_value
        elif operator == ">":
            correct = correct and sol_value and sol_value > eval_value
        elif operator == "<":
            correct = correct and sol_value and sol_value < eval_value
        return {"correct": correct}
