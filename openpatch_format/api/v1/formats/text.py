import re
from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)

from openpatch_format.api.v1.formats import Format


def validate_regex(regex):
    try:
        re.compile(regex)
    except Exception as e:
        raise ValidationError("Not a valid regex")


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    rows = fields.Int()
    multiline = fields.Boolean()


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    regex = fields.Str(validate=validate_regex)


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    value = fields.Str()


class Text(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        correct = True
        regex_pattern = evaluation.get("regex", "")
        solution = SolutionSchema().load(solution)

        prog = re.compile(regex_pattern)
        result = prog.match(solution.get("value", ""))

        correct = result is not None

        return {"correct": correct}
