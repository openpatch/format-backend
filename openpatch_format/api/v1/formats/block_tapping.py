from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.api.v1.formats import Format


class BlockSchema(Schema):
    x = fields.Int(required=True)
    y = fields.Int(required=True)
    width = fields.Int(validate=validate.Range(min=0), required=True)
    height = fields.Int(validate=validate.Range(min=0), required=True)


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    blocks = fields.Nested(BlockSchema, many=True)
    taps = fields.List(fields.Int)
    show_selected = fields.Boolean()

    @validates_schema
    def valiate_taps(self, data, **kwargs):
        for tap in data.get("taps", []):
            try:
                data.get("blocks", [])[tap]
            except Exception:
                raise ValidationError("tap index out of range", "taps")


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    in_order = fields.Boolean()


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    taps = fields.List(fields.Int)


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})


class BlockTapping(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        in_order = evaluation.get("in_order", True)
        if not in_order:
            return {
                "correct": data.get("taps", []).sort()
                == solution.get("taps", []).sort()
            }

        return {"correct": data.get("taps", []) == solution.get("taps", [])}
