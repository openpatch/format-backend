import os
import requests
import javalang
from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from flask import json
from openpatch_format.api.v1 import errors
from openpatch_format.api.v1.formats import Format
from openpatch_format.HmacUtils import sign_and_send


class SourceSchema(Schema):
    file_name = fields.Str()
    source = fields.Str()


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    sources = fields.Nested(SourceSchema, many=True)


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    tests = fields.Nested(SourceSchema, many=True)
    image = fields.Str(default="registry.gitlab.com/openpatch/runner-java:v1.0.0")


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema, required=True)
    evaluation = fields.Nested(EvaluationSchema, default={})


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    sources = fields.Nested(SourceSchema, many=True)


class Code(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        runner_json = {}
        runner_json["image"] = evaluation["image"]
        runner_json["timeout"] = 30
        runner_json["user"] = "openpatch_format"

        return cls.evaluate_code(runner_json, evaluation, solution, data)

    @classmethod
    def make_java_runner_source(cls, file_name, source):
        package_name = None
        try:
            tree = javalang.parse.parse(source)
            package_name = tree.package.name if tree.package else None
            class_name = tree.types[0].name
            fqcn = class_name
        except:
            fqcn = os.path.splitext(file_name)[0]
        if package_name:
            fqcn = package_name + "." + fqcn
        return {"fqcn": fqcn, "code": source}

    @classmethod
    def make_python_runner_source(cls, file_name: str, source: str):
        if file_name.endswith(".py"):
            file_name = file_name[:-3]

        return {"module": file_name, "code": source}

    @classmethod
    def evaluate_code(cls, runner_json, evaluation, solution, data):

        sources = []
        tests = []

        make_runner_source = cls.make_java_runner_source

        if "python" in runner_json["image"]:
            make_runner_source = cls.make_python_runner_source

        for source in solution.get("sources", []):
            sources.append(
                make_runner_source(
                    source.get("file_name", ""), source.get("source", "")
                )
            )

        for test in evaluation.get("tests", []):
            tests.append(make_runner_source(test.get("file_name"), test.get("source")))

        runner_json["payload"] = {"sources": sources, "tests": tests}

        r = requests.Request(
            "POST",
            os.getenv("OPENPATCH_RUNNER_SERVICE") + "/api/v1/run",
            json=runner_json,
        )

        try:
            r = sign_and_send(r, timeout=60)

        except requests.exceptions.ReadTimeout:
            return {"correct": False}

        if not r.status_code == 200:
            return errors.service_not_available()

        r_json = r.json()

        r_payload = r_json.get("payload", {})

        correct = True

        # check if there is a compilation error
        for test_compile in r_payload.get("test_compile", []):
            correct = correct and test_compile.get("exitcode") == 0

        if correct:
            # check if all tests have passed
            for test_result in r_payload.get("test_results", []):
                correct = correct and test_result.get("is_correct", True)

        return {"correct": correct, "details": r_json}
