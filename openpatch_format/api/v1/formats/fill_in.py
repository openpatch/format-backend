import re

from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.api.v1.formats import Format
from openpatch_format.utils.draft import RawDraftContent


def validate_regex(regex):
    try:
        re.compile(regex)
    except Exception as e:
        raise ValidationError("Not a valid regex")


class InputSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    regex = fields.Str(validate=validate_regex)


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    text = fields.Nested(RawDraftContent)


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    inputs = fields.Nested(InputSchema, many=True)


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    values = fields.Dict(keys=fields.Str(), values=fields.Str())


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})


class FillIn(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        correct = True
        details = []
        evaluation = EvaluationSchema().load(evaluation)
        try:
            solution = SolutionSchema().load(solution)
        except ValidationError:
            solution = {}
        inputs = evaluation.get("inputs", [])
        values = solution.get("values", {})

        i = 0
        for input in inputs:
            value = values.get(str(i), "")
            prog = re.compile(input.get("regex", ""))
            result = prog.match(value)

            details.append(result is not None)
            i += 1

        correct = not False in details

        return {"correct": correct, "details": details}
