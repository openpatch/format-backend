from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.api.v1.formats import Format


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    value = fields.Time()
    operator = fields.Str(validate=validate.OneOf(["==", ">=", "<=", ">", "<"]))

    min = fields.Time(allow_none=True)
    max = fields.Time(allow_none=True)

    @validates_schema
    def validate_min_max(self, data, **kwargs):
        value = data.get("value")
        operator = data.get("operator")
        min = data.get("min")
        max = data.get("max")
        if min is not None and max is not None and min > max:
            raise ValidationError("min can not be greate than max", "min")

        if value is not None:
            if min is not None and value < min:
                raise ValidationError("value can not be smaller than min", "value")
            if max is not None and value > max:
                raise ValidationError("value can not be greater than max", "value")
            if operator is None:
                raise ValidationError("operator is needed", "operator")


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    value = fields.Time()


class Time(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        operator = evaluation.get("operator")

        evaluation = EvaluationSchema().load(evaluation)
        solution = SolutionSchema().load(solution)

        min = evaluation.get("min", None)
        max = evaluation.get("max", None)

        correct = True
        if min and solution.get("value") < min:
            correct = False
        if max and solution.get("value") > max:
            correct = False

        if not correct:
            return {"correct": correct}

        if operator == "==":
            correct = evaluation.get("value") == solution.get("value")
        elif operator == ">=":
            correct = solution.get("value") >= evaluation.get("value")
        elif operator == "<=":
            correct = solution.get("value") <= evaluation.get("value")
        elif operator == ">":
            correct = solution.get("value") > evaluation.get("value")
        elif operator == "<":
            correct = solution.get("value") < evaluation.get("value")
        return {"correct": correct}
