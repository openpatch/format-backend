from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.api.v1.formats import Format
from openpatch_format.utils.draft import RawDraftContent


class BlockSchema(Schema):
    linked = fields.Int()
    locked = fields.Boolean()
    text = fields.Nested(RawDraftContent)


class BlockPosition(Schema):
    id = fields.Int()
    indentation = fields.Int()


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    blocks = fields.Nested(BlockSchema, many=True)
    allow_indent = fields.Boolean()


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    positions = fields.Nested(BlockPosition, many=True)
    ignore_indent = fields.Boolean()


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    positions = fields.Nested(BlockPosition, many=True)


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})


class ParsonsPuzzle(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        evaluation = EvaluationSchema.load(evaluation)
        solution = SolutionSchema.load(solution)

        correct = False

        if evaluation.get("ignore_indent"):
            sbp = [b.id for b in solution.get("block_positions", [])]
            ebp = [b.id for b in evaluation.get("block_positions", [])]

            correct = sbp == ebp

        else:
            sbp = [b.id for b in solution.get("block_positions", [])]
            ebp = [b.id for b in evaluation.get("block_positions", [])]

            si = [b.indentation for b in solution.get("block_positions", [])]
            ei = [b.indentation for b in evaluation.get("block_positions", [])]

            correct = sbp == ebp and si == ei

        return {"correct": correct}
