from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.api.v1.formats import Format


class CombinationSchema(Schema):
    upper = fields.Str(required=True)
    middle = fields.Str(required=True)
    lower = fields.Str(required=True)


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    combinations = fields.Nested(CombinationSchema, many=True, required=True)


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)

    correct_combination = fields.Nested(CombinationSchema)


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema, required=True)
    evaluation = fields.Nested(EvaluationSchema, required=True)


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    crossed = fields.List(fields.Int)


class Attention(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        # evaluation = EvaluationSchema.load(evaluation)
        solution = SolutionSchema.load(solution)
        # data = DataSchema.load(data)

        correct = True
        details = []
        correct_combination = evaluation.get("correct_combination")
        combinations = data.get("combinations", [])
        for crossed in solution.get("crossed"):
            crossed_combination = combinations[crossed]
            if (
                crossed_combination.get("upper_symbol")
                == correct_combination.get("upper_symbol")
                and crossed_combination("middle_symbol")
                == correct_combination("middle_symbol")
                and correct_combination("lower_symbol")
                == correct_combination("lower_symbol")
            ):
                details.append(True)
            else:
                correct = False
                details.append(False)

        return {"correct": correct, "details": details}
