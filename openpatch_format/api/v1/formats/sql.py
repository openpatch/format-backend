import json
import os
import requests
import sqlite3
from openpatch_format.HmacUtils import sign_and_send
from openpatch_format.api.v1 import errors
from openpatch_format.api.v1.formats import Format
from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)

# defining schemas for validations


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE


class QuerySchema(Schema):
    class Meta:
        unknown = EXCLUDE

    source = fields.Str(default="", required=True)


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    query1 = fields.Nested(QuerySchema, default={}, required=True)


class SourcesSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    file_name = fields.Str(default="")
    source = fields.Str(default="", required=True)


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    current_source = fields.Integer(required=True)
    sources = fields.List(fields.Nested(SourcesSchema), default=[{}], required=True)


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema, required=True)
    evaluation = fields.Nested(EvaluationSchema, default={})


class Sql(Format):
    @classmethod
    def validate(cls, task):
        """
        Validates the task by checking:
        1. if the task schema is correct
        2. if the provided query1 can be executed without errors
        """

        validate_task_schema_result = cls.validate_task_schema(task)

        # case 1: task schema is invalid --> correct: False
        if validate_task_schema_result.get("correct") == False:
            return {
                "correct": False,
                "details": {"error": validate_task_schema_result.get("error")},
            }
        # case 2: task schema is valid
        else:
            validate_query_result = cls.validate_query(task)
            # casea 2a: task schema is valid but query1 is invalid --> correct: False
            if validate_query_result.get("correct") == False:
                return {
                    "correct": False,
                    "details": {"error": validate_query_result.get("error")},
                    "task": TaskSchema().dump(task),
                }
            # case 2b: task schema is valid and query1 is valid --> correct: True
            else:
                return {"correct": True, "task": TaskSchema().dump(task)}

    @classmethod
    def validate_task_schema(cls, task):
        """
        Validates the task schema by using Marshmallow and the above defined schema
        """

        try:
            task = TaskSchema().load(task)
        except ValidationError as e:
            return {"correct": False, "error": e.messages}
        return {"correct": True}

    @classmethod
    def validate_query(cls, task):
        """
        Validates the query by executing it on the database
        """

        # prepare request to runner
        evaluation = task.get("evaluation")
        if not evaluation:
            query1 = ""
        else:
            query1 = evaluation.get("query1")
            if not query1:
                query1 = ""
            else:
                query1_source = query1.get("source")
                if not query1_source:
                    query1 = ""
                else:
                    query1 = evaluation.get("query1").get("source")

        request_body = {
            "image": "docker.pkg.github.com/laurakeil1/runner-sql/runner-sql:1.0",
            "timeout": 30,
            "user": "openpatch_format",
            "payload": {"payload": {"request_type": "validate", "query1": query1}},
        }

        # get response from runner
        response = cls.request_runner(request_body)

        # handle query validation result
        if not response:
            return {"correct": False, "error": {}}
        else:
            payload = response.get("payload")
            if not payload:
                return {"correct": False, "error": {}}
            else:
                test_results = payload.get("test_results")
                if test_results == True:
                    return {"correct": True}
                else:
                    return {
                        "correct": False,
                        "error": response.get("payload").get("error"),
                    }

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        """
        Evaluates the solution by checking:
        1. if the solution schema is correct
        2. if the result of the solution schema query (query2) matches the result of the evaluation query (query1)
        """

        validate_solution_schema_result = cls.validate_solution_schema(solution)
        # case 1: solution schema is invalid
        if validate_solution_schema_result.get("correct") == False:
            return {
                "correct": False,
                "details": {"error": validate_solution_schema_result.get("error")},
            }
        # case 2: solution schema is valid
        else:
            return cls.evaluate_query(evaluation, solution, data)

    @classmethod
    def validate_solution_schema(cls, solution):
        """
        Validates the solution schema by using Marshmallow and the above defined schema
        """

        try:
            solution = SolutionSchema().load(solution)
        except ValidationError as e:
            return {"correct": False, "error": e.messages}
        return {"correct": True}

    @classmethod
    def evaluate_query(cls, evaluation, solution, data):
        """
        Evaluates the solution query (query2) by comparing its result to the evaluation query (query1) result
        """
        # prepare request to runner

        if not evaluation:
            query1 = ""
        else:
            query1 = evaluation.get("query1")
            if not query1:
                query1 = ""
            else:
                query1_source = query1.get("source")
                if not query1_source:
                    query1 = ""
                else:
                    query1 = evaluation.get("query1").get("source")

        if not solution:
            query2 = ""
        else:
            sources = solution.get("sources")
            if not sources:
                query2 = ""
            else:
                query2 = sources[0].get("source")

        request_body = {
            "image": "docker.pkg.github.com/laurakeil1/runner-sql/runner-sql:1.0",
            "timeout": 30,
            "user": "openpatch_format",
            "payload": {
                "payload": {
                    "request_type": "evaluate",
                    "query1": query1,
                    "query2": query2,
                }
            },
        }

        # get response from runner
        response = cls.request_runner(request_body)

        # handle evaluation result
        if response.get("correct") == False:
            return response
        else:
            if response.get("payload").get("test_results") == True:
                return {"correct": True, "details": response}
            else:
                if response.get("payload").get("error") != None:
                    # put the error message on an higher level
                    response = {
                        "payload": response.get("payload"),
                        "runner_http_status": response.get("runner_http_status"),
                        "error": response.get("payload").get("error"),
                    }
                return {"correct": False, "details": response}

    @classmethod
    def request_runner(cls, request_body):
        """
        Sends request to the runner with provided request_body
        """

        # build request
        r = requests.Request(
            "POST",
            os.getenv("OPENPATCH_RUNNER_SERVICE") + "/api/v1/run",
            json=request_body,
        )

        # send request and handle exceptions
        try:
            r = sign_and_send(r, timeout=60)
        except requests.exceptions.ReadTimeout:
            return {"correct": False, "error": "ReadTimoeout"}

        if not r.status_code == 200:
            return {"correct": False, "error": "Resonse status != 200"}

        # return response
        return r.json()
