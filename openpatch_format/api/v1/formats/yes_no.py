from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.api.v1.formats import Format


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    yes = fields.Boolean()


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    yes = fields.Boolean()


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})


class YesNo(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        evaluation = EvaluationSchema().load(evaluation)
        solution = SolutionSchema().load(solution)

        return {"correct": evaluation.get("yes") == solution.get("yes")}
