from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.api.v1.formats import Format
from openpatch_format.utils.draft import RawDraftContent


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    text = fields.Nested(RawDraftContent)
    view_limit = fields.Int()
    time_limit = fields.Int()


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    cutoff = fields.Int()


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    text = fields.Str()


class Memorize(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}
