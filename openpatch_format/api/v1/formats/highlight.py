import re
from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.api.v1.formats import Format
from openpatch_format.utils import interrater
from openpatch_format.utils.draft import RawDraftContent

color_regex = re.compile("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$")


def validate_color(value):
    match = color_regex.match(value)

    if not match:
        raise ValidationError("not a hex color")


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    text = fields.Nested(RawDraftContent)
    colors = fields.List(fields.Str(validate=validate_color))


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    highlighted_text = fields.Nested(RawDraftContent)
    cutoff = fields.Number()


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    highlighted_text = fields.Nested(RawDraftContent)


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})


class Highlight(Format):
    @classmethod
    def extract_highlighted_ranges(cls, text, colors):
        ranges_per_color = {}

        for color in colors:
            ranges_per_color[color] = {}

        for block in text.get("blocks", []):
            key = block.get("key")
            for color in colors:
                ranges_per_color[color][key] = [False] * len(block.get("text", ""))

            for inline_style_range in block.get("inline_style_ranges", []):
                if inline_style_range.get("style") in colors:
                    offset = inline_style_range.get("offset", 0)
                    length = inline_style_range.get("length", 0)
                    color = inline_style_range.get("style")
                    for i in range(offset, offset + length):
                        ranges_per_color[color][key][i] = True

        flatten_ranges_per_color = {}
        for color, blocks in ranges_per_color.items():
            ranges = []
            for block_ranges in blocks.values():
                ranges += block_ranges
            flatten_ranges_per_color[color] = ranges

        return flatten_ranges_per_color

    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        correct = True
        details = {}

        try:
            solution = SolutionSchema().load(solution)
        except ValidationError:
            solution = {}
        evaluation = EvaluationSchema().load(evaluation)
        data = DataSchema().load(data)
        evaluation_ranges = cls.extract_highlighted_ranges(
            evaluation.get("highlighted_text", {}), data.get("colors", [])
        )
        solution_ranges = cls.extract_highlighted_ranges(
            solution.get("highlighted_text", {}), data.get("colors", [])
        )

        for color in data.get("colors", []):
            color_details = {}
            contingency_table = interrater.get_contingency_table(
                evaluation_ranges.get(color, []), solution_ranges.get(color, [])
            )
            kappa = interrater.get_kappa(contingency_table)

            color_details["contingency_table"] = contingency_table
            color_details["kappa"] = kappa

            details[color] = color_details

            if kappa < evaluation.get("cutoff", -10):
                correct = False

        return {"correct": correct, "details": details}
