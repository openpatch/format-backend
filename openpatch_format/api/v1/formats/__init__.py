class Format:
    """
    Each Format should implement the two classmethods below. To make
    use of a default Implementation each format should inherit this
    class.
    """

    @classmethod
    def validate(cls, task):
        """
        It should return correct = True, if the task is valid.
        It should return correct = False, if not.
        It also can return a details object describing the errors
        """
        return {"correct": True}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        """
        It should return correct = True, if the solution is correct.
        It should return correct = False, if the solution is incorrect.
        The evaluation should be based on the evaluation and the data object.
        It also can return a details object with more detailed information.
        """
        return {"correct": solution == evaluation}
