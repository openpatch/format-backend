from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.api.v1.formats import Format


class SpotSchema(Schema):
    shape = fields.Str(required=True, validate=validate.OneOf(["rectangle", "ellipse"]))
    x = fields.Int(required=True)
    y = fields.Int(required=True)
    height = fields.Int(required=True)
    width = fields.Int(required=True)


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    spots = fields.Nested(SpotSchema, many=True, validate=validate.Length(min=1))
    img = fields.Str()


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    needed = fields.Int(validate=validate.Range(min=0))


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})

    @validates_schema
    def validate_needed(self, data, **kwargs):
        task_data = data.get("data", {})
        if task_data.get("needed", 0) > len(task_data.get("spots", [])):
            raise ValidationError("more needed than spots", "needed")


def test_ellipse(x, y, h, w, cx, cy):
    return ((cx - x) ** 2 / (w / 2.0) ** 2) + ((cy - y) ** 2 / (h / 2.0) ** 2) <= 1


def test_rectangle(x, y, h, w, cx, cy):
    return cx > x and cx < x + w and cy > y and cy < y + h


class Hotspot(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        correct = True

        details = []

        spots = data.get("spots", [])
        needed = evaluation.get("needed", 0)
        number_clicks = evaluation.get("number_clicks", None)

        user_clicks = solution.get("clicks", [])

        for spot in spots:
            details.append(False)
            shape = spot.get("shape", "ellipse")
            x = spot.get("x", 0)
            y = spot.get("y", 0)
            height = spot.get("height", 0)
            width = spot.get("width", 0)

            for click in user_clicks:
                test = test_rectangle
                if shape == "ellipse":
                    test = test_ellipse
                inside = test(x, y, height, width, click.get("x", 0), click.get("y", 0))

                if inside:
                    details[-1] = True
                    break

        correct = any(details) >= needed

        return {"correct": correct, "details": details}
