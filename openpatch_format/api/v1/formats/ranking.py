from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.api.v1.formats import Format
from openpatch_format.utils.draft import RawDraftContent


class BlockSchema(Schema):
    id = fields.UUID()
    linked = fields.Int()
    locked = fields.Boolean()
    text = fields.Nested(RawDraftContent)


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    blocks = fields.Nested(BlockSchema, many=True)


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    positions = fields.List(fields.Int())


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    positions = fields.List(fields.Int())
    source_positions = fields.List(fields.Int())
    path = fields.List(fields.List(fields.Int()))


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})


class Ranking(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        evaluation = EvaluationSchema().load(evaluation)
        solution = SolutionSchema().load(solution)

        return {
            "correct": evaluation.get("positions", []) == solution.get("positions", [])
        }
