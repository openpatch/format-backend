from openpatch_format.api.v1.formats import Format
from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
from openpatch_format.utils.draft import RawDraftContent


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    allow_multiple = fields.Boolean()
    choices = fields.Nested(RawDraftContent, many=True)


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    choices = fields.Dict(keys=fields.Str(), values=fields.Boolean())


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema)
    evaluation = fields.Nested(EvaluationSchema, default={})

    @validates_schema
    def validate_multiple(self, data, **kwargs):
        evaluation = data.get("evaluation", {})
        data = data.get("data", {})

        if (
            not data.get("allow_multiple", False)
            and not len(evaluation.get("choices", [])) <= 1
        ):
            raise ValidationError("only one choice if not allow multiple", "choices")

    @validates_schema
    def validate_evaluation(self, data, **kwargs):
        evaluation = data.get("evaluation", {})
        data = data.get("data", {})

        for key in evaluation.get("choices", {}).keys():
            try:
                if not int(key) < len(data.get("choices", [])):
                    raise ValidationError("key out of bounds", "evaluation.choices")
            except Exception as e:
                raise ValidationError("key out of bounds", "evaluation.choices")


class Choice(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        solution_choices = solution.get("choices", {})
        solution_choices = {k: v for k, v in solution_choices.items() if v}

        evaluation_choices = evaluation.get("choices", {})
        evaluation_choices = {k: v for k, v in evaluation_choices.items() if v}

        correct = evaluation_choices == solution_choices
        return {"correct": correct}
