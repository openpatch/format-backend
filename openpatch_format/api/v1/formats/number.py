from decimal import Decimal
from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    EXCLUDE,
)
import re

from openpatch_format.api.v1.formats import Format


class DataSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    min = fields.Number()
    max = fields.Number()
    steps = fields.Number()
    upper_label = fields.Str()
    lower_label = fields.Str()

    @validates_schema
    def validate_min_max(self, data, **kwargs):
        min = data.get("min")
        max = data.get("max")
        steps = data.get("steps")
        if min and max and min > max:
            raise ValidationError("min can not be greate than max", "min")

        if min is not None and max is not None:
            if steps is None:
                raise ValidationError("a step is needed", "steps")
            if (min * 1000) % (steps * 1000) != 0:
                # fix floating point error
                raise ValidationError("min must be a multiple of steps")
            if (max * 1000) % (steps * 1000) != 0:
                raise ValidationError("max must be a multiple of steps")


class EvaluationSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    skip = fields.Boolean(default=True)
    value = fields.Number()
    operator = fields.Str(validate=validate.OneOf(["==", ">=", "<=", ">", "<"]))


class TaskSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    data = fields.Nested(DataSchema, required=True)
    evaluation = fields.Nested(EvaluationSchema, default={})

    @validates_schema
    def validate_value(self, data, **kwargs):
        evaluation = data.get("evaluation", {})
        data = data.get("data", {})

        value = evaluation.get("value")
        operator = evaluation.get("operator")
        min = data.get("min")
        max = data.get("max")
        steps = data.get("steps")

        if value is not None:
            if min is not None and value < min:
                raise ValidationError("value can not be smaller than min", "data.value")
            if max is not None and value > max:
                raise ValidationError("value can not be greater than max", "data.value")
            if steps is not None and (value * 1000) % (steps * 1000) != 0:
                raise ValidationError("value must be a multiple of steps", "data.value")
            if operator is None:
                raise ValidationError("operator is needed", "operator")


class SolutionSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    value = fields.Number()


class Number(Format):
    @classmethod
    def validate(cls, task):
        try:
            task = TaskSchema().load(task)
            return {"correct": True, "task": TaskSchema().dump(task)}
        except ValidationError as e:
            return {"correct": False, "details": e.messages}

    @classmethod
    def evaluate(cls, evaluation, solution, data):
        operator = evaluation.get("operator")

        evaluation = EvaluationSchema().load(evaluation)
        solution = SolutionSchema().load(solution)

        correct = False
        if operator == "==":
            correct = evaluation.get("value") == solution.get("value")
        elif operator == ">=":
            correct = solution.get("value") >= evaluation.get("value")
        elif operator == "<=":
            correct = solution.get("value") <= evaluation.get("value")
        elif operator == ">":
            correct = solution.get("value") > evaluation.get("value")
        elif operator == "<":
            correct = solution.get("value") < evaluation.get("value")
        return {"correct": correct}
