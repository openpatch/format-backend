# Format Service

## Environment Variables

| Environment Variable | Description | Default | Values |
| -------------------- | ----------- | ------- | ------ |
| OPENPATCH_MODE | Configures the configuration | "producation" | "development", "production", "testing" |
| OPENPATCH_RUNNER_SERVICE | url to runner service | http://runner | a url |
| OPENPATCH_ORIGINS | Allowed CORS origins | "*" | valid cors origin |
| SENTRY_DSN | Error tracking with Sentry | not set | a valid dsn |

## Setup

You need to have [docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/) installed.

For running the service, the migrations and the database:

```
docker network create openpatch-runner-net
docker-compose up
```

## Testing

Test everything
```
docker-compose run --rm -e OPENPATCH_MODE="testing" backend flask test
```

Test a single module
```
docker-compose run --rm -e OPENPATCH_MODE="testing" backend python3 -m unittest tests.api.v1.formats.test_choice
```

## Coverage

```
docker-compose run --rm -e OPENPATCH_MODE="testing" backend python3 coverage_report.py
```

