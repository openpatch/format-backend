# [1.6.0](https://gitlab.com/openpatch/format-backend/compare/v1.5.0...v1.6.0) (2021-01-11)


### Features

* add python support ([1eadcdd](https://gitlab.com/openpatch/format-backend/commit/1eadcdd9045a90be9ccad6eac40edbdf5711e8aa))

# [1.5.0](https://gitlab.com/openpatch/format-backend/compare/v1.4.1...v1.5.0) (2021-01-11)


### Features

* sql format ([e1dbefe](https://gitlab.com/openpatch/format-backend/commit/e1dbefef14f65e488e3703aad52077ba30eb093c))
