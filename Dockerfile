FROM registry.gitlab.com/openpatch/flask-microservice-base:v3.7.0

COPY "." "/var/www/app"

ENV OPENPATCH_SERVICE_NAME=format
ENV OPENPATCH_RUNNER_SERVICE=http://runner
ENV OPENPATCH_ORIGINS=*

RUN pip3 install -r /var/www/app/requirements.txt
